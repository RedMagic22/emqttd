##修改内容
修改来自emqttd：https://github.com/emqtt/emqttd
现初步实现了对publish的基本信息的存取，以及系统内部交流的mqtt_message消息（如心跳包）存取。如仅仅只需查询客户端之间的通讯的publish的历史，那么在mysql查询时增加限制条件即可。
编译前需要先修改src目录下postpublish.erl内的mysql的用户名等信息。